# dividendo = 12
# divisor = 0
#
# resultado = dividendo / divisor

# dividendo = 12
# divisor = 0
#
# try:
#     resultado = dividendo / divisor
# except:
#     print("No es posible dividir por 0 un número")
#
# try:
#     #Código con posibles excepciones
#
# except IOError:
#     #Excepciones de entrada/salida (IOError)
#
# except ZeroDivisionError:
#     #Excepciones de división por cero (ZeroDivisionError)
#
# except:
#     #Excepciones de cualquier otro tipo distinto a los anteriores


# try:
#     archivo = open("archivo.txt")
#
# except IOError:
#     # Excepciones de entrada/salida (IOError)
#     print('Error de entrada/salida.')
#
# except:
#     #Excepciones no tratadas
#     print('Error no tratado')
#
# finally:
#     # Si el archivo no está cerrado lo cerramos
#     if not (archivo.closed):
#         archivo.close()

import sys


def dividir(dividendo, divisor):

    try:
        resultado = dividendo / divisor
        return resultado

    except ZeroDivisionError as err:
        print('ZeroDivisionError:', err)

    except:
        print("Error no esperado:", sys.exc_info()[0])
        raise


resultado = dividir(5,0)


dividendo = 12
divisor = 0

try:
    resultado = dividendo / divisor
except:
    print("No es posible dividir por 0 un número")


def dividir(dividendo, divisor):

    try:
        resultado = dividendo / divisor
        return resultado

    except ZeroDivisionError as err:
        print('ZeroDivisionError:', err)

    except:
        print("Error no esperado:", sys.exc_info()[0])
        raise