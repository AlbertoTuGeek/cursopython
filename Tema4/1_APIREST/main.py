from flask import Flask
import json
from flask import request

app = Flask(__name__)

libros = []
cont_id = 0

@app.route("/")
def hello():
    data_api = {"api": "rest ^_^"}

    return json.dumps(data_api)


@app.route("/auth/login", methods=["POST"])
def log_auth():

    #curl -d '{"usuario":"admin", "clave":"top_secret"}' -H "Content-Type: application/json" -X POST http://localhost:8888/auth/login

    values = request.get_json()

    if values['usuario'] == 'admin' and values['clave'] == 'top_secret':
        respuesta = {'error': False, 'message': 'User logged'}
    else:
        respuesta = {'error': True, 'message': 'Fail Auth'}

    return json.dumps(respuesta)


@app.route("/app/api/post", methods=["POST"])
def api_post():
    global cont_id

    cont_id += 1

    # curl -d '{"titulo":"Titulo 1", "detalles":"Detalles Libro"}' -H "Content-Type: application/json" -X POST http://localhost:8888/app/api/post

    values = request.get_json()

    tit = values["titulo"]
    det = values["detalles"]

    if tit != "":
        libros.append({
            "id": cont_id,
            "titulo": tit,
            "detalles": det
        })
        response = libros
    else:
        response = {'error': True, 'message': 'Debe indicar al menos un titulo'}

    return json.dumps(response)


@app.route("/app/api/post/random", methods=["POST", "GET"])
def api_post_random():
    global cont_id

    for i in range(10):
        cont_id += 1
        # Generamos un poco de contenido
        libros.append({
            "id": cont_id,
            "titulo": "Titulo "+str(cont_id),
            "detalles": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        })

    return json.dumps(libros)


@app.route("/app/api/get", methods=["GET"])
def api_get():
    return json.dumps(libros)


@app.route("/app/api/get/<id_libro>/", methods=["GET"])
def details_get(id_libro):
    try:

        libro = [libro for libro in libros if libro.get("id") == int(id_libro)][0]

    except IndexError:
        error = {'error': True, 'message': 'Not Found'}
        return json.dumps(error)

    return json.dumps(libro)


@app.route("/app/api/put", methods=["PUT"])
def api_put():
    try:
        # curl -d '{"id":2, "titulo":"Titulo Modificado", "detalles":"Detalles Modificados"}' -H "Content-Type: application/json" -X PUT http://localhost:8888/app/api/put
        values = request.get_json()

        id_libro = values["id"]
        tit = values["titulo"]
        det = values["detalles"]

        libro = [libro for libro in libros if libro.get("id") == int(id_libro)][0]

        libro.update({
            "titulo": tit,
            "detalles": det
        })
    except IndexError:
        error = {'error': True, 'message': 'Not Found'}
        return json.dumps(error)

    return json.dumps(libro)


@app.route("/app/api/delete/<id_libro>/", methods=["DELETE"])
def api_delete(id_libro):
    # curl -d '' -H "Content-Type: application/json" -X DELETE http://localhost:8888/app/api/delete/1/

    try:
        libro = [libro for libro in libros if libro.get("id") == int(id_libro)][0]

        libros.remove(libro)
    except IndexError:
        error = {'error': True, 'message': 'Not Found'}
        return json.dumps(error)

    return json.dumps(libro)

if __name__ == "__main__":
    app.run(debug=True, port=8888)