from cuenta import Cuenta
from persona import Persona


class CuentaAhorro(Cuenta):
    def __init__(self, nombre, apellidos, numeroCuenta, cantidad, fecha, tipo=0.0):
        Cuenta.__init__(self, numeroCuenta, cantidad)
        self.__tipoInteres = 1 + tipo
        self.__fechaApertura = fecha
        self.__persona = Persona(nombre, apellidos)

    def getTipoInteres(self):
        return self.__tipoInteres

    def setTipoInteres(self, interes):
        self.__tipoInteres = interes

    def getFecha(self):
        return self.__fechaApertura

    def getSaldo(self):
        return self._saldo*self.__tipoInteres

    def setNombre(self, nombre):
        self.__persona.setNombre(nombre)

    def getNombre(self):
        return self.__persona.getNombre()

    def setApellido(self, apellido):
        self.__persona.setApellido(apellido)

    def getApellido(self):
        return self.__persona.getApellido()



class CuentaCorriente(Cuenta):
    def __init__(self, nombre, apellidos, numeroCuenta, cantidad, tipo=0.0,tarjetaDebito=False,tarjetaCredito=False,cuota=0.0):
        Cuenta.__init__(self, numeroCuenta, cantidad)
        self.__tipoInteres = 1 + tipo
        self.__tarjetaDebito = tarjetaDebito
        self.__tarjetaCredito = tarjetaCredito
        self.__cuotaMantenimiento = cuota
        self.__persona = Persona(nombre, apellidos)

    def getTipoInteres(self):
        return self.__tipoInteres

    def setTipoInteres(self, interes):
        self.__tipoInteres = interes

    def getTarjetaDebito(self):
        return self.__tarjetaDebito

    def setTarjetaDebito(self, tarjeta):
        self.__tarjetaDebito = tarjeta

    def getTarjetaCredito(self):
        return self.__tarjetaCredito

    def setTarjetaCredito(self, tarjeta):
        self.__tarjetaCredito = tarjeta

    def getCuotaMantenimiento(self):
        return self.__cuotaMantenimiento

    def setCuotaMantenimiento(self, cuota):
        self.__cuotaMantenimiento = cuota

    def sacarDinero(self, cantidad):
        self._saldo -= cantidad*self.__tipoInteres
        print("Ha retirado", cantidad)
        print("Comisión", cantidad*self.__tipoInteres-cantidad)
        print("Su saldo es", self._saldo)

    def setNombre(self, nombre):
        self.__persona.setNombre(nombre)

    def getNombre(self):
        return self.__persona.getNombre()

    def setApellido(self, apellido):
        self.__persona.setApellido(apellido)

    def getApellido(self):
        return self.__persona.getApellido()
