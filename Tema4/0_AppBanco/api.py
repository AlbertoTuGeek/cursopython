from flask import Flask
import json
from datos import Datos

app = Flask(__name__)

datos = Datos("cuentas.dat")

@app.route("/api/transf/cuenta/<cuenta>/cant/<cantidad>/", methods=["GET"])
def transf_put(cuenta, cantidad):
    datos.load()

    clientes = datos.clientes

    try:
        cliente = [cliente for cliente in clientes if cliente.getNumeroCuenta() == int(cuenta)][0]

        cliente.ingresarDinero(int(cantidad))
    except:
        result = {"result": False, "message": "Error realizando la transferencia"}
        return json.dumps(result)

    result = {"result": True, "message": "Transferencia correcta"}

    datos.save()

    return json.dumps(result)

if __name__ == "__main__":
    app.run(debug= True, port= 8000)