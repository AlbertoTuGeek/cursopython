import pickle


class Datos:
    def __init__(self, nom_fichero):
        self.__file_name = nom_fichero
        self.clientes = []

    def load(self):
        try:
            with open(self.__file_name, 'rb') as f:
                self.clientes = pickle.load(f)
        except:
            f = open('cuentas.dat', 'wb')
            f.close()

        with open(self.__file_name, 'wb') as f:
            pickle.dump(self.clientes, f)

    def save(self):
        with open(self.__file_name, 'wb') as f:
            pickle.dump(self.clientes, f)
