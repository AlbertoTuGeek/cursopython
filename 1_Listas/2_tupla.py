tupla = ("abc", 42, 3.1415)
print(tupla[0]) # Acceder a un elemento por su índice
del tupla[0] # No es posible borrar (ni añadir) un elemento en una tupla, lo que provocará una excepción
tupla[0] = "xyz" # Tampoco es posible re-asignar el valor de un elemento en una tupla, lo que también provocará una excepción
print(tupla[0:2]) # Mostrar los elementos de la tupla del índice "0" al "2" (sin incluir este último)
tupla_anidada = (tupla, (True, 3.1415)) # También es posible anidar tuplas
print(1, 2, 3, "abc") # Esto también es una tupla, aunque es recomendable ponerla entre paréntesis (recuerda que requiere, al menos, una coma)
print((1))  # Aunque entre paréntesis, esto no es una tupla, ya que no posee al menos una coma, por lo que únicamente aparecerá el valor
print((1,))  # En cambio, en este otro caso, sí es una tupla
print((1, 2))  # Con más de un elemento no es necesaria la coma final
print((1, 2,))  # Aunque agregarla no modifica el resultado