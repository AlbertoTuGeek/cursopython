# from tkinter import *
#
# import logging
# logging.basicConfig(level=logging.DEBUG)
#
# logging.debug('Informacion de Debug')
# logging.info('Mensaje de información')
# logging.warning('Warning:el fichero de configuracion %s no se ha encontrado', 'server.conf')
# logging.error('Error')
# logging.critical('Error critico')
#

>>> from decimal import *
>>> round(Decimal('0.70') * Decimal('1.05'), 2)
Decimal('0.74')
>>> round(.70 * 1.05, 2)
0.73
>>> Decimal('1.00') % Decimal('.10')
Decimal('0.00')
>>> 1.00 % 0.10
0.09999999999999995
>>> sum([Decimal('0.1')]*10) == Decimal('1.0')
True
>>> sum([0.1]*10) == 1.0
False