l = [1, 2, 3]

l2 = [n ** 2 for n in l]

print(l2)

l2 = [n for n in l if n % 2.0 == 0]

print(l2)

l = [0, 1, 2, 3]
m = ["a", "b"]
n = [s * v for s in m for v in l if v > 1]

print(n)

l2 = (n ** 2 for n in l)
print(l2)


def mi_generador(n, m, s):
    while(n <= m):
        yield n
        n += s

x = mi_generador(0, 5, 1)

print(x)

print(list(x))

for n in mi_generador(0, 5, 2):
    print(n)

def mi_decorador(funcion):
    def nueva(*args):
        print("Llamada a la funcion", funcion.__name__)
        retorno = funcion(*args)
        return retorno
    return nueva


def imp(s):
    print(s)

imp("hola")

mi_decorador(imp)("hola")

@otro_decorador
@mi_decorador
def imp(s):
    print(s)

imp("hola")
