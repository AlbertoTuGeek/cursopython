def saludar(lang):
    def saludar_es():
        print("Hola")
        
    def saludar_en():
        print("Hi")
        
    def saludar_fr():
        print("Salut")
        
    lang_func = {
        "es": saludar_es,
        "en": saludar_en,
        "fr": saludar_fr
    }

    return lang_func[lang]


def cuadrado(n):
    return n ** 2

l = [1, 2, 3]
l2 = map(cuadrado, l)

print(list(l2))

def es_par(n):
    return (n % 2.0 == 0)

l = [1, 2, 3]
l2 = filter(es_par, l)

print(list(l2))