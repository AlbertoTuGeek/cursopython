import requests


# response = requests.get("http://www.google.com")

url = "https://www.googleapis.com/books/v1/volumes?q=python"

response = requests.get(url)

if response.status_code == 200:
    results = response.json()

    for result in results['items']:
        print(result['volumeInfo']['title'])
else:
    print("Error code %s" % response.status_code)


# datos = {
#     "nombre": "Usuario",
#     "correo": "usuario@prueba.com"
# }
#
# url = "http://example.com/api/v1/alta_usuario"
# response = requests.post(url, data=datos)


datos = {
    "nombre": "Usuario",
    "correo": "usuario@prueba.com"
}

cabeceras = {
    "Content-Type": "application/json",
    "Accept": "application/json"
}

galletas = {
    "cookie1": "aquí va la cookie"
}

url = "http://example.com/api/v1/alta_usuario"
response = requests.post(url,
                         data=datos,
                         headers=cabeceras,
                         cookies=galletas
                         )
